/*
 * SPDX-FileCopyrightText: 2020-2024 Aurora OSS
 * SPDX-FileCopyrightText: 2023-2024 The Calyx Institute
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import java.util.Properties

// Signing configuration
val localProperties = Properties().apply {
    val localPropertiesFile = rootProject.file("local.properties")
    if (localPropertiesFile.exists()) {
        load(localPropertiesFile.inputStream())
    }
}

val tokenUsername: String? = localProperties.getProperty("ossrhUsername")
val tokenPassword: String? = localProperties.getProperty("ossrhPassword")
val shouldSignRelease: Boolean
    get() = !tokenUsername.isNullOrEmpty() && !tokenPassword.isNullOrEmpty()

plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.jetbrains.kotlin.android)
    alias(libs.plugins.jetbrains.kotlin.parcelize)
    alias(libs.plugins.ktlint)
    alias(libs.plugins.protobuf)
    `maven-publish`
    signing
}

val versionMajor = 3
val versionMinor = 4
val versionPatch = 6
val releasePatch = 0

val versionName = "${versionMajor}.${versionMinor}.${versionPatch}-${releasePatch}"

kotlin {
    jvmToolchain(21)
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

android {
    namespace = "com.aurora.gplayapi"
    compileSdk = 35
    version = versionName

    defaultConfig {
        minSdk = 21
        aarMetadata {
            minCompileSdk = 21
        }
        consumerProguardFiles("proguard-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_21
        targetCompatibility = JavaVersion.VERSION_21
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_21.toString()
    }
    packaging {
        resources {
            excludes += "**/*.proto"
        }
    }
}

dependencies {

    implementation(libs.google.gson)
    implementation(libs.google.protobuf)
    implementation(libs.squareup.okhttp)
    implementation(libs.kotlin.coroutines.android)
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:${libs.versions.protobufJavaliteVersion.get()}"
    }
    generateProtoTasks {
        all().forEach { task ->
            task.builtins {
                create("java") {
                    option("lite")
                }
            }
        }
    }
}

// Run "./gradlew publishReleasePublicationToLocalRepository" to generate release AAR locally
publishing {
    publications {
        afterEvaluate {
            create<MavenPublication>("release") {
                groupId = "foundation.e"
                artifactId = "gplayapi"
                version = versionName
                from(components["release"])
            }
        }
    }

    repositories {
        val ciJobToken = System.getenv("CI_JOB_TOKEN")
        val ciApiV4Url = System.getenv("CI_API_V4_URL")
        if (ciJobToken != null) {
            maven {
                url = uri("${ciApiV4Url}/projects/1269/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = ciJobToken
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        } else {
            maven {
                url = uri("https://gitlab.e.foundation/api/v4/projects/1269/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Private-Token"
                    value = System.getenv("gitLabPrivateToken")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }

        maven {
            name = "local"
            url = uri("./build/repo")
        }
    }
}
