package com.aurora.gplayapi.helpers

import com.aurora.gplayapi.GooglePlayApi
import com.aurora.gplayapi.ListResponse
import com.aurora.gplayapi.data.models.AuthData
import com.aurora.gplayapi.data.models.StreamCluster
import com.aurora.gplayapi.data.providers.HeaderProvider
import com.aurora.gplayapi.helpers.contracts.TopChartsContract
import com.aurora.gplayapi.network.IHttpClient
import java.util.HashMap

class CategoryAppsHelper(authData: AuthData) : NativeHelper(authData) {

    private val streamCluster = ClusterHelper(authData)

    override fun using(httpClient: IHttpClient) = apply {
        this.httpClient = httpClient
        streamCluster.using(httpClient)
    }

    fun getCategoryAppsList(category: String): StreamCluster {
        val headers: MutableMap<String, String> = HeaderProvider.getDefaultHeaders(authData)
        val params: MutableMap<String, String> = HashMap()
        params["c"] = "3"
        params["cat"] = category
        params["ctr"] = TopChartsContract.Chart.TOP_SELLING_FREE.value

        val playResponse = httpClient.get(GooglePlayApi.LIST, headers, params)

        return if (playResponse.isSuccessful) {
            val listResponse: ListResponse = getResponseFromBytes(playResponse.responseBytes)
            val streamCluster = getStreamCluster(listResponse)
            streamCluster
        } else {
            StreamCluster()
        }
    }

    fun next(nextPageUrl: String): StreamCluster {
        return streamCluster.next(nextPageUrl)
    }
}