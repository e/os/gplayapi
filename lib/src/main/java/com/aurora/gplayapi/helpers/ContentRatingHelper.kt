/*
 * Copyright (C) 2024 MURENA SAS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.aurora.gplayapi.helpers

import com.aurora.gplayapi.DetailsResponse
import com.aurora.gplayapi.GooglePlayApi
import com.aurora.gplayapi.data.builders.AppBuilder
import com.aurora.gplayapi.data.models.AuthData
import com.aurora.gplayapi.data.models.ContentRating
import com.aurora.gplayapi.data.providers.HeaderProvider.getDefaultHeaders
import com.aurora.gplayapi.exceptions.InternalException
import com.aurora.gplayapi.network.IHttpClient
import java.util.Locale

class ContentRatingHelper(authData: AuthData) : NativeHelper(authData) {

    companion object {
        private const val HTTP_HEADER_ACCEPT_LANGUAGE = "Accept-Language"
        private const val HTTP_QUERY_PARAM_DOC = "doc"
    }

    override fun using(httpClient: IHttpClient) = apply {
        this.httpClient = httpClient
    }

    fun updateContentRatingWithId(
        appPackage: String,
        currentContentRating: ContentRating,
    ): ContentRating {
        // If the device's language is English, the default rating will be in English.
        // So, its title can already be treated as an identifier for the rating.
        if (Locale.getDefault().language == Locale.ENGLISH.language) {
            return currentContentRating.copy(id = currentContentRating.title.lowercase())
        }

        return try {
            val englishLocaleContentRating = getEnglishContentRating(appPackage)

            // For rating in other languages, we set the English title as its ID
            currentContentRating.copy(id = englishLocaleContentRating.title.lowercase())
        } catch (appNotFoundException: InternalException.AppNotFound) {
            currentContentRating
        }
    }

    fun getEnglishContentRating(appPackage: String): ContentRating {
        val response = getAppDetails(appPackage, Locale.US)
        val contentRating = AppBuilder.build(response).contentRating
        return contentRating.copy(id = contentRating.title.lowercase())
    }

    private fun getAppDetails(
        packageName: String, locale: Locale = Locale.getDefault()
    ): DetailsResponse {
        val headers = buildHeaders(locale)
        val params = mapOf(HTTP_QUERY_PARAM_DOC to packageName)
        val playResponse = httpClient.get(GooglePlayApi.URL_DETAILS, headers, params)

        return if (playResponse.isSuccessful) {
            val detailsResponse: DetailsResponse = getResponseFromBytes(playResponse.responseBytes)
            detailsResponse
        } else {
            throw InternalException.AppNotFound(playResponse.errorString)
        }
    }

    private fun buildHeaders(locale: Locale): Map<String, String> {
        return getDefaultHeaders(authData).apply {
            this[HTTP_HEADER_ACCEPT_LANGUAGE] = locale.toLanguageTag()
        }
    }
}
