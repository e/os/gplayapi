/*
 * SPDX-FileCopyrightText: 2020-2021 Aurora OSS
 * SPDX-FileCopyrightText: 2023-2024 The Calyx Institute
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package com.aurora.gplayapi.data.models

data class PlayResponse(
    var responseBytes: ByteArray = byteArrayOf(),
    var errorBytes: ByteArray = byteArrayOf(),
    var errorString: String = ("No Error"),
    var isSuccessful: Boolean = false,
    var code: Int = 0
)
